# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

POURSUITE()
import numpy as np

# import matplotlib.pyplot as plt
from code_aster.Objects.table_py import Table

# [1] Shen, Y.; Vizzaccaro, A.; Kesmia N.; Yu T.; Salle L.; Thomas O.; Touzé C.
# Comparison of reduction methods for finite element geometrically nonlinear beam structures.
# Vibration 2021, 4, 175-204.
# [2] Vizzaccaro, A.; Shen, Y.; Salles, L.; Blahos, J.; Touzé, C.
# Direct computation of nonlinear mapping via normal form for reduced-order models of finite element nonlinear structures.
# Comput. Methods Appl. Mech. Eng. 2020, submitted for publication.

# Function of non-linear stiffness forces
def R_non(x, dx, L, A, B, CEX):
    FR = np.zeros([L])
    for m in range(0, L):
        for j1 in range(0, L):
            for j2 in range(0, L):
                for j3 in range(0, L):
                    temp1 = A[m, j1, j2, j3] * x[j1] * x[j2] * x[j3]
                    temp2 = B[m, j1, j2, j3] * x[j1] * dx[j2] * dx[j3]
                    temp3 = CEX[m, j1, j2, j3] * x[j1] * x[j2] * dx[j3]
                    FR[m] = FR[m] + temp1 + temp2 + temp3
    return FR


########################################################################
#                            Parametres                                #

# coefficients pour une matrice d’amortissement visqueux
# proportionnel à la rigidité et la masse
amor_beta = 0.0
amor_alpha = 1.0622e-5

# time step and total time for the simulation
nstep = 15000
ttotal = 3
tstep = 1.0 * ttotal / nstep
time = np.arange(0, ttotal + tstep, tstep)

# parameters for the newmark
beta = 1.0 / 4
gamma = 1.0 / 2
nmax = 30
NRtol = 1e-15

# define the external excitation
Fexcit = np.zeros([N_modes, nstep + 1])
amp = 300
ex = 1.02 * omega[0]  #  960.2
dy0 = 1e-4


# The expressions c_ij up to first order
# for the tensor c̄ with the assumption of Rayleigh damping in [1] eq. 34
CEX = -2 * amor_alpha * (AH_modal - H_modal)
for j in range(0, N_modes):
    for k in range(0, N_modes):
        CEX[:, :, j, k] = (
            CEX[:, :, j, k] + (amor_beta + 3 * omega[j] ** 2 * amor_alpha) * B_modal[:, :, j, k]
        )
        CEX[:, :, j, k] = CEX[:, :, j, k] + (-amor_beta + 2 * omega[j] ** 2 * amor_alpha) * (
            Cs_modal[:, :, j, k] + Cd_modal[:, :, j, k]
        )
        CEX[:, :, j, k] = CEX[:, :, j, k] + (-amor_beta + 2 * omega[k] ** 2 * amor_alpha) * omega[
            j
        ] / omega[k] * (Cs_modal[:, :, j, k] - Cd_modal[:, :, j, k])


# mass matrix M, stiffness matrix K, damping matrix C
M_ = np.eye(N_modes)
K_ = np.diag(omega) ** 2
C_ = amor_beta * M_ + amor_alpha * K_

K_hat = 1.0 / beta / tstep**2 * M_ + gamma / beta / tstep * C_ + K_


phi = np.zeros([N_modes])
phiV = np.zeros([N_modes])
numnode = mesh.getNodes("NC")  # numero 311 commence par 1 => 310 commence par 0
for m in range(0, N_modes):
    phi[m] = EigenModes[m][(numnode[0] + 1) * 3 - 1]
    # excited position: DZ
    phiV[m] = EigenModes[m][(numnode[0] + 1) * 3 - 2]
    # excited position: DY

########################################################################
#                            Newmark Beta                              #

# initial condition
x = np.zeros([N_modes, nstep + 1])
dx = np.zeros([N_modes, nstep + 1])
ddx = np.zeros([N_modes, nstep + 1])
R = np.zeros([N_modes, nstep + 1])
D_f = np.zeros([N_modes, nstep + 1])
F_hat = np.zeros([N_modes, nstep + 1, nmax])
D_dx = np.zeros([N_modes, nstep + 1, nmax])
D_x = np.zeros([N_modes, nstep + 1, nmax])
D_R = np.zeros([N_modes, nstep + 1, nmax])

# initial displacement : DY - 2nd mode
x[1, 0] = dy0 / phiV[1]


# sinus force
for tf in range(0, nstep + 1):
    for m in range(0, N_modes):
        Fexcit[m, tf] = amp * phi[m] * np.sin(ex * tf / nstep * ttotal)


# Main procedure of Newmark_beta
for i in range(0, nstep):
    D_f[:, i] = Fexcit[:, i + 1] - Fexcit[:, i]
    F_hat[:, i, 0] = (
        D_f[:, i]
        + np.dot((1.0 / beta / 2 * M_ - tstep * (1 - gamma / 2 / beta) * C_), ddx[:, i])
        + np.dot((1.0 / beta / tstep * M_ + gamma / beta * C_), dx[:, i])
    )

    # inicial guess for NR.
    D_x[:, i, 0] = np.zeros([1, N_modes])

    for n in range(0, nmax):
        D_dx[:, i, n] = (
            gamma / beta / tstep * D_x[:, i, n]
            - gamma / beta * dx[:, i]
            + tstep * (1 - gamma / 2 / beta) * ddx[:, i]
        )
        D_R[:, i, n] = R_non(
            x[:, i] + D_x[:, i, n], dx[:, i] + D_dx[:, i, n], N_modes, AH_modal, B_modal, CEX
        ) - R_non(x[:, i], dx[:, i], N_modes, AH_modal, B_modal, CEX)
        F_hat[:, i, n] = F_hat[:, i, 0] - D_R[:, i, n]
        D_x[:, i, n + 1] = np.dot(np.linalg.inv(K_hat), F_hat[:, i, n])
        if (abs(D_x[:, i, n + 1] - D_x[:, i, n]) < NRtol).all():
            break
        else:
            continue

    x[:, i + 1] = x[:, i] + D_x[:, i, n]
    dx[:, i + 1] = (
        (1 - gamma / beta) * dx[:, i]
        + tstep * (1 - gamma / 2 / beta) * ddx[:, i]
        + gamma / beta / tstep * D_x[:, i, n]
    )
    R_next = R_non(x[:, i + 1], dx[:, i + 1], N_modes, AH_modal, B_modal, CEX)
    ddx[:, i + 1] = np.dot(
        -np.linalg.inv(M_),
        (np.dot(C_, dx[:, i + 1]) + np.dot(K_, x[:, i + 1]) + R_next - Fexcit[:, i + 1]),
    )


# displacement results
dy = x[0, :] * phiV[0] + x[1, :] * phiV[1]
dz = x[0, :] * phi[0] + x[1, :] * phi[1]


########################################################################
#                     comparaison                                      #

# full FEM results
# t_ext, dy_ext, dz_ext = np.loadtxt( '/home/H80575/Documents/2021/2021_04/Yichang/perf901a/perf901b.txt',usecols=(0,1,2),unpack=True,skiprows=1)
tab_ext = LIRE_TABLE(UNITE=31, FORMAT="ASTER")
t_ext = tab_ext.EXTR_TABLE().values()["time"]
dy_ext = tab_ext.EXTR_TABLE().values()["dy"]
dz_ext = tab_ext.EXTR_TABLE().values()["dz"]

# # temporal response comparison
#
# figure1=plt.figure(1)
# plt.plot(time,dz,'b-',label='DNF', linewidth=0.1)
# plt.plot(t_ext,dz_ext,'r-',label='FEM', linewidth=0.1)
# plt.xlabel('temps (s)');plt.ylabel('DZ')
# plt.xlim(0,1)
# plt.legend(loc=1)
# #plt.show()
#
# figure2=plt.figure(2)
# plt.plot(time,dy,'b-',label='DNF', linewidth=0.1)
# plt.plot(t_ext,dy_ext,'r-',label='FEM', linewidth=0.1)
# plt.xlabel('temps (s)');plt.ylabel('DY')
# plt.xlim(0,1)
# plt.legend(loc=1)
# #plt.show()
#
# figure1.savefig('/home/H80575/Documents/2021/2021_04/Yichang/perf901a/fig_z.eps', format='eps')
# figure2.savefig('/home/H80575/Documents/2021/2021_04/Yichang/perf901a/fig_y.eps', format='eps')


listdic = [
    {"time": time[4000], "dy": dy[4000], "dz": dz[4000]},
    {"time": time[8000], "dy": dy[8000], "dz": dz[8000]},
    {"time": time[12000], "dy": dy[12000], "dz": dz[12000]},
]

listpara = ["time", "dy", "dz"]
listtype = ["R", "R", "R"]
tab = Table(listdic, listpara, listtype)

motcles = tab.dict_CREA_TABLE()
tab_depl = CREA_TABLE(TYPE_TABLE="TABLE", **motcles)


# tester the values en comparant avec FEM
TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=0.8),),
    NOM_PARA="dy",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dy_ext[4000],
    PRECISION=5.0e-2,
    REFERENCE="AUTRE_ASTER",
)

TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=1.6),),
    NOM_PARA="dy",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dy_ext[8000],
    PRECISION=10.0e-2,
    REFERENCE="AUTRE_ASTER",
)


TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=2.4),),
    NOM_PARA="dy",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dy_ext[12000],
    PRECISION=5.0e-2,
    REFERENCE="AUTRE_ASTER",
)


TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=0.8),),
    NOM_PARA="dz",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dz_ext[4000],
    PRECISION=4.0e-2,
    REFERENCE="AUTRE_ASTER",
)


TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=1.6),),
    NOM_PARA="dz",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dz_ext[8000],
    PRECISION=8.0e-2,
    REFERENCE="AUTRE_ASTER",
)


TEST_TABLE(
    TABLE=tab_depl,
    FILTRE=(_F(NOM_PARA="time", VALE=2.4),),
    NOM_PARA="dz",
    CRITERE="RELATIF",
    VALE_CALC=1.0,  # ignorer dans test validation
    VALE_REFE=dz_ext[12000],
    PRECISION=5.0e-2,
    REFERENCE="AUTRE_ASTER",
)

FIN(INFO_RESU="NON")
