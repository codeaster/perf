# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

#       INTERET : CALCULER 10 MODES EN POSITION 11, 12, ..., 20
# ---------------------------------------------------------------------------
# VARIANTE DU CAS TEST PERF003C RAFFINE UNIFORMEMENT AVEC HOMARD ET
# UTILISANT LE SOLVEUR MUMPS.
# VARIANTE SEQUENTIELLE.
# ICI ON PRIVILEGIE LA MEMOIRE SUR LE TEMPS: D'OU LE CHOIX RENUM=METIS +
# GESTION_MEMOIRE='OUT_OF_CORE'.
# ICI L'OPTION MATR_DISTRIBUEE N'A PAS LIEU D'ETRE.
# ---------------------------------------------------------------------------

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"), MESURE_TEMPS=_F(NIVE_DETAIL=2))

MA = LIRE_MAILLAGE(FORMAT="MED")

MODEL = AFFE_MODELE(
    MAILLAGE=MA,
    DISTRIBUTION=_F(METHODE="CENTRALISE"),
    AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="DKT"),
)

CAREL = AFFE_CARA_ELEM(MODELE=MODEL, COQUE=_F(GROUP_MA="ABCD", EPAIS=0.02))
MAT1 = DEFI_MATERIAU(ELAS=_F(E=5.0e11, NU=0.3, RHO=9800.0))
CHMAT = AFFE_MATERIAU(MAILLAGE=MA, AFFE=_F(TOUT="OUI", MATER=MAT1))
BLOQ = AFFE_CHAR_MECA(
    MODELE=MODEL,
    DDL_IMPO=(
        _F(GROUP_MA=("AB"), DX=0.0, DY=0.0, DZ=0.0, DRX=0.0, DRY=0.0, DRZ=0.0),
        _F(GROUP_MA=("DA"), DX=0.0, DY=0.0, DZ=0.0, DRX=0.0, DRY=0.0, DRZ=0.0),
    ),
)


RIGIELEM = CALC_MATR_ELEM(
    CHARGE=BLOQ, OPTION="RIGI_MECA", CARA_ELEM=CAREL, MODELE=MODEL, CHAM_MATER=CHMAT
)
MASSELEM = CALC_MATR_ELEM(
    CHARGE=BLOQ, OPTION="MASS_MECA", CARA_ELEM=CAREL, MODELE=MODEL, CHAM_MATER=CHMAT
)
NUME = NUME_DDL(MATR_RIGI=RIGIELEM)
RIGI = ASSE_MATRICE(NUME_DDL=NUME, MATR_ELEM=RIGIELEM)
MASS = ASSE_MATRICE(NUME_DDL=NUME, MATR_ELEM=MASSELEM)
MODES = CALC_MODES(
    INFO=2,
    CALC_FREQ=_F(FREQ=(840.0, 1600.0)),
    VERI_MODE=_F(SEUIL=1.0e-05),
    OPTION="BANDE",
    SOLVEUR=_F(RENUM="QAMD", METHODE="MUMPS", GESTION_MEMOIRE="OUT_OF_CORE"),
    MATR_RIGI=RIGI,
    MATR_MASS=MASS,
    SOLVEUR_MODAL=_F(METHODE="SORENSEN"),
    TYPE_RESU="DYNAMIQUE",
)

TEST_RESU(
    RESU=_F(
        NUME_ORDRE=3,
        PARA="FREQ",
        REFERENCE="NON_DEFINI",
        RESULTAT=MODES,
        VALE_CALC=993.671961370,
        VALE_REFE=993.5,
        CRITERE="RELATIF",
        PRECISION=3.0000000000000001e-3,
    )
)

FIN()
