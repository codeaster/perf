# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# 5e5 ELEMENTS LINEAIRES
#       INTERET : MESURER LES PERFORMANCES D'UN CALCUL ELASTIQUE 3D MASSIF
# ---------------------------------------------------------------------------

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"))

MA = LIRE_MAILLAGE(FORMAT="MED")

MO = AFFE_MODELE(MAILLAGE=MA, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D"))
E_REF = 5.0e11
E = DEFI_FONCTION(
    INFO=1,
    INTERPOL="LIN",
    NOM_PARA="TEMP",
    NOM_RESU="TOUTRESU",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(0.0, E_REF, 1.0, E_REF),
    VERIF="CROISSANT",
)


NU = DEFI_CONSTANTE(VALE=0.3)
RHO = DEFI_CONSTANTE(VALE=9800.0)
ALPHA = DEFI_CONSTANTE(VALE=1.0e-6)

MAT = DEFI_MATERIAU(ELAS_FO=_F(E=E, NU=NU, RHO=RHO, ALPHA=ALPHA, TEMP_DEF_ALPHA=0.0))
PI = pi


FCHAR = FORMULE(VALE="cos(Z/PI)", NOM_PARA="Z", PI=PI)

CHGEOM = CREA_CHAMP(TYPE_CHAM="NOEU_GEOM_R", OPERATION="EXTR", MAILLAGE=MA, NOM_CHAM="GEOMETRIE")

CHFONC = CREA_CHAMP(
    TYPE_CHAM="NOEU_NEUT_F",
    OPERATION="AFFE",
    MODELE=MO,
    AFFE=_F(TOUT="OUI", NOM_CMP="X1", VALE_F=FCHAR),
)

CHTEMPF = CREA_CHAMP(TYPE_CHAM="NOEU_NEUT_R", OPERATION="EVAL", CHAM_F=CHFONC, CHAM_PARA=CHGEOM)

CHTEMP = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R",
    OPERATION="ASSE",
    MODELE=MO,
    ASSE=_F(TOUT="OUI", CHAM_GD=CHTEMPF, NOM_CMP="X1", NOM_CMP_RESU="TEMP"),
)

# nombre de pas de temps
npas = 20
dt = 1.0 / npas
CHTEMP_VAR = []

# on crée les différents champs de TEMP
for i in range(npas + 1):
    time = i * dt
    new_temp = CHTEMP
    CHTEMP_VAR.append({"NOM_CHAM": "TEMP", "INST": time, "CHAM_GD": new_temp})

LINST = DEFI_LIST_REEL(DEBUT=0.0, INTERVALLE=_F(JUSQU_A=1.0, NOMBRE=npas))


TEMP = CREA_RESU(OPERATION="AFFE", TYPE_RESU="EVOL_THER", AFFE=CHTEMP_VAR)

CHMAT = AFFE_MATERIAU(
    MAILLAGE=MA,
    AFFE=_F(TOUT="OUI", MATER=MAT),
    AFFE_VARC=_F(TOUT="OUI", NOM_VARC="TEMP", EVOL=TEMP, VALE_REF=0.0),
)

CHAR = AFFE_CHAR_CINE(
    MODELE=MO,
    MECA_IMPO=(
        _F(GROUP_NO="POINT_A", DX=0.0, DY=0.0, DZ=0.0),
        _F(GROUP_NO="POINT_B", DX=0.0, DY=0.0),
        _F(GROUP_NO="POINT_C", DY=0.0),
    ),
)

mumps = {"METHODE": "MUMPS", "LOW_RANK_SEUIL": 0.0, "PCENT_PIVOT": 20, "RESI_RELA": 1.0e-06}


petsc_cg = {
    "ALGORITHME": "CG",
    "MATR_DISTRIBUEE": "OUI",
    "METHODE": "PETSC",
    "PRE_COND": "LDLT_SP",
    "REAC_PRECOND": 30,
    "RENUM": "SANS",
    "RESI_RELA": 1e-08,
}

petsc_gm = {
    "ALGORITHME": "GMRES",
    "MATR_DISTRIBUEE": "OUI",
    "METHODE": "PETSC",
    "NMAX_ITER": 0,
    "PRE_COND": "BOOMER",
    "RESI_RELA": 1e-08,
}

solvers = [petsc_cg, petsc_gm]

for solver in solvers:
    RESU = MECA_STATIQUE(
        MODELE=MO,
        CHAM_MATER=CHMAT,
        EXCIT=_F(CHARGE=CHAR),
        OPTION="SIEF_ELGA",
        SOLVEUR=solver,
        LIST_INST=LINST,
        INFO=1,
    )

    TEST_RESU(
        RESU=(
            _F(
                INST=1,
                REFERENCE="AUTRE_ASTER",
                TYPE_TEST="MAX",
                RESULTAT=RESU,
                NOM_CHAM="DEPL",
                NOM_CMP="DX",
                VALE_CALC=5.78517104359775e-05,
                VALE_REFE=5.78517104359775e-05,
                CRITERE="RELATIF",
                PRECISION=1.0000000000000001e-5,
            ),
            _F(
                INST=1,
                REFERENCE="AUTRE_ASTER",
                TYPE_TEST="MAX",
                RESULTAT=RESU,
                NOM_CHAM="DEPL",
                NOM_CMP="DY",
                VALE_CALC=2.8925855217798128e-05,
                VALE_REFE=2.8925855217798128e-05,
                CRITERE="RELATIF",
                PRECISION=1.0000000000000001e-5,
            ),
        )
    )

if False:  # pour faire une belle image :

    RESU = CALC_CHAMP(
        reuse=RESU, RESULTAT=RESU, CRITERES=("SIEQ_ELNO"), CONTRAINTE=("SIEF_ELGA", "SIGM_ELNO")
    )

    RESU = CALC_CHAMP(reuse=RESU, RESULTAT=RESU, CRITERES="SIEQ_NOEU")

    IMPR_RESU(
        FORMAT="MED", UNITE=38, RESU=_F(MAILLAGE=MA, RESULTAT=RESU, NOM_CHAM=("DEPL", "SIEQ_NOEU"))
    )

FIN()
