# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

#       INTERET : MESURER LES PERFORMANCES D'UN CALCUL ELASTO-PLASTIQUE 3D
# ---------------------------------------------------------------------------

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"))

MA = LIRE_MAILLAGE(FORMAT="MED")

MA = MODI_MAILLAGE(reuse=MA, MAILLAGE=MA, ORIE_PEAU=_F(GROUP_MA_PEAU=("P1")))

MO = AFFE_MODELE(MAILLAGE=MA, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D"))


MAT1 = DEFI_MATERIAU(
    ELAS=_F(E=5.0e11, NU=0.3, RHO=9800.0), ECRO_LINE=_F(D_SIGM_EPSI=1.0e4, SY=1.0e9)
)


CHMAT = AFFE_MATERIAU(MAILLAGE=MA, AFFE=_F(TOUT="OUI", MATER=MAT1))

CHAR1 = AFFE_CHAR_MECA(
    MODELE=MO,
    PRES_REP=_F(GROUP_MA="P1", PRES=2.0e6),
    DDL_IMPO=(
        _F(GROUP_NO=("A",), DX=0.0, DY=0.0, DZ=0.0),
        _F(GROUP_NO=("A2"), DY=0.0, DZ=0.0),
        _F(GROUP_NO=("X1"), DZ=0.0),
    ),
)

RAMPE = DEFI_FONCTION(NOM_PARA="INST", VALE=(0.0, 0.0, 20.0, 20.0))
LINST1 = DEFI_LIST_REEL(
    DEBUT=0.0, INTERVALLE=(_F(JUSQU_A=9.0, NOMBRE=1), _F(JUSQU_A=10.0, NOMBRE=4))
)


RESU = STAT_NON_LINE(
    MODELE=MO,
    CHAM_MATER=CHMAT,
    INFO=1,
    SOLVEUR=_F(NPREC=9, METHODE="MULT_FRONT"),
    EXCIT=(_F(CHARGE=CHAR1, FONC_MULT=RAMPE),),
    INCREMENT=_F(LIST_INST=LINST1),
    # COMPORTEMENT=_F(  RELATION = 'ELAS'),
    COMPORTEMENT=_F(RELATION="VMIS_ISOT_LINE"),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    # RECH_LINEAIRE=_F( ITER_LINE_MAXI = 10),
    CONVERGENCE=_F(ARRET="OUI", ITER_GLOB_MAXI=20, ITER_GLOB_ELAS=25),
)

TEST_RESU(
    RESU=_F(
        NUME_ORDRE=3,
        GROUP_NO="X2",
        REFERENCE="NON_DEFINI",
        RESULTAT=RESU,
        NOM_CHAM="DEPL",
        NOM_CMP="DZ",
        VALE_CALC=5.52247761e-03,
        VALE_REFE=5.4999999999999997e-3,
        CRITERE="RELATIF",
        PRECISION=0.02,
    )
)

if 0:
    # pour faire une belle image :
    RESU = CALC_CHAMP(
        reuse=RESU,
        RESULTAT=RESU,
        CRITERES=("SIEQ_ELNO"),
        VARI_INTERNE=("VARI_ELNO"),
        CONTRAINTE=("SIEF_ELNO"),
    )

    RESU = CALC_CHAMP(reuse=RESU, RESULTAT=RESU, CRITERES="SIEQ_NOEU", VARI_INTERNE="VARI_NOEU")

    IMPR_RESU(
        FORMAT="MED",
        UNITE=38,
        RESU=_F(MAILLAGE=MA, RESULTAT=RESU, NOM_CHAM=("DEPL", "SIEQ_NOEU", "VARI_NOEU")),
    )

if 0:
    # pour evaluer le % de PG plastifies, on compte ceux qui ont V2=1.0 :
    X1 = CREA_CHAMP(
        OPERATION="EXTR",
        TYPE_CHAM="ELGA_VARI_R",
        INFO=2,
        NOM_CHAM="VARI_ELGA",
        RESULTAT=RESU,
        NUME_ORDRE=5,
    )


FIN()
