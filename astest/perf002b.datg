# -*- coding: utf-8 -*-
import math
import salome
salome.salome_init()
import GEOM
from salome.geom import geomBuilder
geompy = geomBuilder.New(salome.myStudy)
import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)

#############Chemin pour l'export du maillage resultat############
ExportPATH="/home/PHIMECA/maillages/"
##################################################################

# dimensions géométriques :
# --------------------------
# epais : épaisseur du tube
# rint  : rayon intérieur du tube
# long1 : longueur des parties droites
# rcoud : rayon du petit coude

epais=0.02
rint=1.0
long1=15.0
rcoud=3.

# nombre d'éléments :
# ---------------------
# n1 : dans l'épaisseur du tube
# n2 : dans la demi-circonférence du tube --> quart circonférence
# n3 : dans les parties droites
# n4 : dans le petit cercle
# n5 : dans le grand cercle

#***********************************************************
# Différents maillages possibles :
#***********************************************************
#   2 250 ddls en CUB8 :
n1=2; n2=5/2 ; n3=6 ; n4=5 ; n5=8 ;

# 30 000 ddls en CUB8 :
n1=3; n2=15/2; n3=21; n4=16; n5=26;

# 50 000 ddls en CUB8 :
n1=3; n2=15/2; n3=30; n4=30; n5=50;

# 100 000 ddls en CUB8 :
n1=4; n2=10; n3=33; n4=30; n5=70;

# 250 000 ddls en CUB8 :
n1=3; n2=15; n3=62; n4=75; n5=150;

# 500 000 ddls en CU20 :
n1=3; n2=12; n3=60; n4=40; n5=80;

# 500 000 ddls en CUB8 :
n1=3; n2=15; n3=125; n4=150; n5=300;

# 1 000 000 ddls en CUB8 :
n1=7; n2=15; n3=125; n4=150; n5=300;

# 2 000 000 ddls en CUB8 :
n1=7; n2=30; n3=125; n4=150; n5=300;
#***********************************************************
# 500 000 ddls en CUB8 :
n1=3; n2=15; n3=125; n4=150; n5=300;
#***********************************************************



pi=math.pi

alpha=75.*pi/180.
long2=long1/(math.sin(alpha))
alpha2=-pi/2.+2.*alpha

O = geompy.MakeVertex(0., 0., 0.)
OX= geompy.MakeVectorDXDYDZ(1., 0., 0.)
OY = geompy.MakeVectorDXDYDZ(0., 1., 0.)
OZ = geompy.MakeVectorDXDYDZ(0., 0., 1.)
planXZ = geompy.MakePlane(O,OY,200)

#
# creation de la section par rotation d'un segment
# avec quatre secteurs  pour recuperer les points CL
#

a1 = geompy.MakeVertex(0., rint, 0.)
b = geompy.MakeVertex(0., (rint+epais), 0.)
a1b = geompy.MakeLineTwoPnt(a1, b)

s0= geompy.MakeRevolution(a1b, OZ, pi/2.)
s1=geompy.MakeRevolution(a1b, OZ, -1.*pi/2.)
s10=geompy.MakeShell([s0,s1])
s11=geompy.MakeMirrorByPlane(s10,planXZ)
section=geompy.MakeShell([s10,s11])

# creation du cercle interieur par rotation d'un point
# pour groupe peau interne
#
c0 = geompy.MakeRevolution(a1, OZ, pi/2.)
c1 = geompy.MakeRevolution(a1, OZ, -1.*pi/2.)
c10 = geompy.MakeWire([c0,c1])
c11=geompy.MakeMirrorByPlane(c10,planXZ)
circonf=geompy.MakeWire([c10,c11])
#
# creation du chemin mousqueton (L)  sur l'axe de la section
#

PL1 = geompy.MakeVertex(0., 0., long1)
L1 = geompy.MakeLineTwoPnt(O,PL1)

z1= geompy.MakeVertex(-10., rcoud, long1)
z2= geompy.MakeVertex( 10., rcoud, long1)
z1z2=geompy.MakeLineTwoPnt(z1, z2)
PL2 = geompy.MakeRotation(PL1, z1z2, (-2.*alpha))
PC2 = geompy.MakeVertex(0., rcoud, long1)
L2 = geompy.MakeArcCenter(PC2,PL1,PL2,0)

z3= -1.*long1*(math.sin(alpha2))
z4= long1*(math.cos(alpha2))
PL3= geompy.MakeTranslation(PL2, 0., z4, z3)
L3 = geompy.MakeLineTwoPnt(PL2,PL3)

y1= long2*(math.cos(alpha))
y3=rcoud+y1
Py1=geompy.MakeVertex(-10., y3, 0.)
Py2=geompy.MakeVertex(10., y3, 0.)
PC4=geompy.MakeVertex(0., y3, 0.)
Py1Py2=geompy.MakeLineTwoPnt(Py1,Py2)
PL4=geompy.MakeRotation(PL3, Py1Py2, (2.*alpha-2.*pi))
L4 = geompy.MakeArcCenter(PC4,PL3,PL4,1)

L = geompy.MakeWire([L1,L2,L3,L4])

#
# Extrusion section selon chemin L
#
coud = geompy.MakePipe(section,L)
#
# Partition par faces intermédiaires pour maillage hexa
#
section1 = geompy.MakeTranslationVector(section,L1)
section2 = geompy.MakeRotation(section1,z1z2, (-2.*alpha))
section3 = geompy.MakeTranslationVector(section2,L3)
coud1= geompy.MakePartition([coud],[section,section1,section2,section3])
geompy.addToStudy(coud1,"coud1")
#
# Creation des groupes geometriques pour le maillage
# par generation de la generatrice issue du point b
#
b1 = geompy.MakeTranslationVector(b,L1)
L1_T = geompy.MakeLineTwoPnt(b,b1)
b2 = geompy.MakeRotation(b1,z1z2, (-2.*alpha))
L2_T = geompy.MakeArcCenter(PC2,b1,b2,0)
b3=geompy.MakeTranslationVector(b2,L3)
L3_T = geompy.MakeLineTwoPnt(b2,b3)
L4_T = geompy.MakeArcCenter(PC4,b3,b,1)

GL1 = geompy.GetInPlace(coud1,L1_T)
GL2 = geompy.GetInPlace(coud1,L2_T)
GL3 = geompy.GetInPlace(coud1,L3_T)
GL4 = geompy.GetInPlace(coud1,L4_T)
ep = geompy.GetInPlace(coud1,a1b)

geompy.addToStudyInFather(coud1,GL1,"GL1")
geompy.addToStudyInFather(coud1,GL2,"GL2")
geompy.addToStudyInFather(coud1,GL3,"GL3")
geompy.addToStudyInFather(coud1,GL4,"GL4")
geompy.addToStudyInFather(coud1,ep,"ep")
#
# Creation des groupes geometriques pour Aster
#
Peau = geompy.MakePipe(circonf,L)
P1 = geompy.GetInPlace(coud1,Peau)

geompy.addToStudyInFather(coud1,P1,"P1")

X1 = geompy.GetPoint(coud1,0., rint, 0., 1.E-7)
geompy.addToStudyInFather(coud1,X1,"X1")

A = geompy.GetPoint(coud1,rint, 0., 0., 1.E-7)
geompy.addToStudyInFather(coud1,A,"A")

A2 = geompy.GetPoint(coud1,(0.-rint), 0., 0., 1.E-7)
geompy.addToStudyInFather(coud1,A2,"A2")

X2 = geompy.GetPoint(coud1, rint, 0., long1, 1.E-7)
geompy.addToStudyInFather(coud1,X2,"X2")

#
# Maillage
#
mesh = smesh.Mesh(coud1,"coud1")

A_1D = mesh.Segment()
A_1D.NumberOfSegments(n2)
A_1D.Propagation()
A_2D = mesh.Quadrangle()
A_3D = mesh.Hexahedron()

B_1D = mesh.Segment(ep)
B_1D.NumberOfSegments(n1)
B_1D.Propagation()

C_1D = mesh.Segment(GL1)
C_1D.NumberOfSegments(n3)
C_1D.Propagation()

D_1D = mesh.Segment(GL2)
D_1D.NumberOfSegments(n4)
D_1D.Propagation()


F_1D = mesh.Segment(GL3)
F_1D.NumberOfSegments(n3)
F_1D.Propagation()

E_1D = mesh.Segment(GL4)
E_1D.NumberOfSegments(n5)
E_1D.Propagation()


mesh.Compute()
P1 = mesh.Group(P1)
A = mesh.Group(A)
A2 = mesh.Group(A2)
X1 = mesh.Group(X1)
X2 = mesh.Group(X2)


########Export du maillage au format MMED########
mesh.ExportMED( r''+ExportPATH+'perf002b.mmed'+'', 0, SMESH.MED_V2_2 , 1 )

