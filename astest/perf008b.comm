# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# 5e5 ELEMENTS QUADRATIQUES
#       INTERET : MESURER LES PERFORMANCES D'UN CALCUL ELASTIQUE 3D MASSIF
# ---------------------------------------------------------------------------

import os.path as osp
from math import pi

from code_aster.Commands import *
from code_aster import CA

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"))

test = CA.TestCase()

MA = LIRE_MAILLAGE(FORMAT="MED")

MO = AFFE_MODELE(MAILLAGE=MA, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D"))

MAT = DEFI_MATERIAU(ELAS=_F(E=5.0e11, NU=0.3, RHO=9800.0, ALPHA=1.0e-6))
PI = pi


FCHAR = FORMULE(VALE="cos(Z/PI)", NOM_PARA="Z", PI=PI)

CHGEOM = CREA_CHAMP(TYPE_CHAM="NOEU_GEOM_R", OPERATION="EXTR", MAILLAGE=MA, NOM_CHAM="GEOMETRIE")

CHFONC = CREA_CHAMP(
    TYPE_CHAM="NOEU_NEUT_F",
    OPERATION="AFFE",
    MODELE=MO,
    AFFE=_F(TOUT="OUI", NOM_CMP="X1", VALE_F=FCHAR),
)

CHTEMPF = CREA_CHAMP(TYPE_CHAM="NOEU_NEUT_R", OPERATION="EVAL", CHAM_F=CHFONC, CHAM_PARA=CHGEOM)

CHTEMP = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R",
    OPERATION="ASSE",
    MODELE=MO,
    ASSE=_F(TOUT="OUI", CHAM_GD=CHTEMPF, NOM_CMP="X1", NOM_CMP_RESU="TEMP"),
)

CHMAT = AFFE_MATERIAU(
    MAILLAGE=MA,
    AFFE=_F(TOUT="OUI", MATER=MAT),
    AFFE_VARC=_F(TOUT="OUI", NOM_VARC="TEMP", CHAM_GD=CHTEMP, VALE_REF=0.0),
)

CHAR = AFFE_CHAR_MECA(
    MODELE=MO,
    DDL_IMPO=(
        _F(GROUP_NO="POINT_A", DX=0.0, DY=0.0, DZ=0.0),
        _F(GROUP_NO="POINT_B", DX=0.0, DY=0.0),
        _F(GROUP_NO="POINT_C", DY=0.0),
    ),
)

RESU = MECA_STATIQUE(
    MODELE=MO,
    CHAM_MATER=CHMAT,
    EXCIT=_F(CHARGE=CHAR),
    OPTION="SANS",
    SOLVEUR=_F(METHODE="MUMPS"),
    INFO=1,
)

TEST_RESU(
    RESU=(
        _F(
            NUME_ORDRE=1,
            REFERENCE="AUTRE_ASTER",
            TYPE_TEST="MAX",
            RESULTAT=RESU,
            NOM_CHAM="DEPL",
            NOM_CMP="DX",
            VALE_CALC=6.45162336283844e-05,
            VALE_REFE=6.45162336283844e-05,
            CRITERE="RELATIF",
            PRECISION=1.0000000000000001e-5,
        ),
        _F(
            NUME_ORDRE=1,
            REFERENCE="AUTRE_ASTER",
            TYPE_TEST="MAX",
            RESULTAT=RESU,
            NOM_CHAM="DEPL",
            NOM_CMP="DY",
            VALE_CALC=3.2258116814413504e-05,
            VALE_REFE=3.2258116814413504e-05,
            CRITERE="RELATIF",
            PRECISION=1.0000000000000001e-5,
        ),
    )
)

if 0:  # pour faire une belle image :
    RESU = CALC_CHAMP(
        reuse=RESU, RESULTAT=RESU, CRITERES=("SIEQ_ELNO"), CONTRAINTE=("SIEF_ELGA", "SIGM_ELNO")
    )

    RESU = CALC_CHAMP(reuse=RESU, RESULTAT=RESU, CRITERES="SIEQ_NOEU")

    IMPR_RESU(
        FORMAT="MED", UNITE=38, RESU=_F(MAILLAGE=MA, RESULTAT=RESU, NOM_CHAM=("DEPL", "SIEQ_NOEU"))
    )

# vérification que le découpage des bases fonctionne
test.assertTrue(osp.exists("glob.1"), msg="glob.1 must exist")

FIN()
