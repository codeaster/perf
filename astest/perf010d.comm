# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# Sequentiel
# Mumps Out-Of-Core (GESTION_MEMOIRE='OUT_OF_CORE')

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"), IMPR_MACRO="OUI")

MA = LIRE_MAILLAGE(FORMAT="MED")

MO = AFFE_MODELE(MAILLAGE=MA, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D"))

MAT = DEFI_MATERIAU(ELAS=_F(E=5.0e11, NU=0.3, RHO=9800.0, ALPHA=1.0e-6))
PI = pi

FCHAR = FORMULE(VALE="cos(Z/PI)", NOM_PARA="Z", PI=PI)

CHGEOM = CREA_CHAMP(TYPE_CHAM="NOEU_GEOM_R", OPERATION="EXTR", MAILLAGE=MA, NOM_CHAM="GEOMETRIE")

CHFONC = CREA_CHAMP(
    TYPE_CHAM="NOEU_NEUT_F",
    OPERATION="AFFE",
    MODELE=MO,
    AFFE=_F(TOUT="OUI", NOM_CMP="X1", VALE_F=FCHAR),
)

CHTEMPF = CREA_CHAMP(TYPE_CHAM="NOEU_NEUT_R", OPERATION="EVAL", CHAM_F=CHFONC, CHAM_PARA=CHGEOM)

CHTEMP = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R",
    OPERATION="ASSE",
    MODELE=MO,
    ASSE=_F(TOUT="OUI", CHAM_GD=CHTEMPF, NOM_CMP="X1", NOM_CMP_RESU="TEMP"),
)

CHMAT = AFFE_MATERIAU(
    MAILLAGE=MA,
    AFFE=_F(TOUT="OUI", MATER=MAT),
    AFFE_VARC=_F(TOUT="OUI", NOM_VARC="TEMP", CHAM_GD=CHTEMP, VALE_REF=0.0),
)

CHAR = AFFE_CHAR_MECA(
    MODELE=MO,
    DDL_IMPO=(
        _F(GROUP_NO="POINT_A", DX=0.0, DY=0.0, DZ=0.0),
        _F(GROUP_NO="POINT_B", DX=0.0, DY=0.0),
        _F(GROUP_NO="POINT_C", DY=0.0),
    ),
)

RESU = MECA_STATIQUE(
    MODELE=MO,
    CHAM_MATER=CHMAT,
    EXCIT=_F(CHARGE=CHAR),
    OPTION="SANS",
    SOLVEUR=_F(METHODE="MUMPS", GESTION_MEMOIRE="OUT_OF_CORE", RESI_RELA=-1.0),
    INFO=2,
)

TEST_RESU(
    RESU=(
        _F(
            NUME_ORDRE=1,
            REFERENCE="AUTRE_ASTER",
            TYPE_TEST="MAX",
            RESULTAT=RESU,
            NOM_CHAM="DEPL",
            NOM_CMP="DX",
            VALE_CALC=5.173794242593425e-05,
            VALE_REFE=5.173794242593425e-05,
            CRITERE="RELATIF",
            PRECISION=1.0e-5,
        ),
        _F(
            NUME_ORDRE=1,
            REFERENCE="AUTRE_ASTER",
            TYPE_TEST="MAX",
            RESULTAT=RESU,
            NOM_CHAM="DEPL",
            NOM_CMP="DY",
            VALE_CALC=2.586897121306346e-05,
            VALE_REFE=2.586897121306346e-05,
            CRITERE="RELATIF",
            PRECISION=1.0e-5,
        ),
    )
)

FIN()
