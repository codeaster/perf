# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

#       INTERET : MESURER LES PERFORMANCES D'UN CALCUL DE CONTACT 3D
#       (ISSU DE SSNV104I)
#       METHODE CONTINUE
# ---------------------------------------------------------------------------

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"))

MA = LIRE_MAILLAGE(FORMAT="MED")

# _____________________________________________________________________
#
#                    DEFINITION DU MODELE
# _____________________________________________________________________

MO = AFFE_MODELE(MAILLAGE=MA, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D"))

MA = MODI_MAILLAGE(reuse=MA, MAILLAGE=MA, ORIE_PEAU=_F(GROUP_MA_PEAU=("SESC", "SMAI")))

# _____________________________________________________________________
#
#                    DEFINITION DES MATERIAUX
# _____________________________________________________________________

MAT = DEFI_MATERIAU(ELAS=_F(E=2.0e4, NU=0.3))

CHMAT = AFFE_MATERIAU(MAILLAGE=MA, AFFE=_F(TOUT="OUI", MATER=MAT))

# _____________________________________________________________________
#
#                    AFFECTATION DES CONDITIONS AUX LIMITES
#                       ET DES DEPLACEMENTS IMPOSES
# _____________________________________________________________________

CHA1 = AFFE_CHAR_MECA(
    MODELE=MO,
    DDL_IMPO=(
        _F(GROUP_NO="FACESUP", DY=-2.0),
        _F(GROUP_NO="FACEINF", DY=2.0),
        _F(GROUP_NO="LIM1", DX=0.0),
        _F(GROUP_NO="LIM2", DZ=0.0),
    ),
)

CHA2 = DEFI_CONTACT(
    MODELE=MO,
    FORMULATION="CONTINUE",
    RESI_GEOM=0.05,
    ALGO_RESO_CONT="POINT_FIXE",
    ALGO_RESO_GEOM="POINT_FIXE",
    ZONE=_F(GROUP_MA_MAIT="SMAI", GROUP_MA_ESCL="SESC", CONTACT_INIT="INTERPENETRE"),
)

L_INST = DEFI_LIST_REEL(DEBUT=0.0, INTERVALLE=_F(JUSQU_A=1.0, NOMBRE=1))

RAMPE = DEFI_FONCTION(
    NOM_PARA="INST", VALE=(0.0, 0.0, 1.0, 1.0), PROL_DROITE="LINEAIRE", PROL_GAUCHE="LINEAIRE"
)

# _____________________________________________________________________
#
#                          RESOLUTION
# _____________________________________________________________________
#

RESU = STAT_NON_LINE(
    SOLVEUR=_F(METHODE="GCPC", NIVE_REMPLISSAGE=1),
    MODELE=MO,
    CHAM_MATER=CHMAT,
    CONTACT=CHA2,
    EXCIT=(_F(CHARGE=CHA1, FONC_MULT=RAMPE)),
    COMPORTEMENT=_F(RELATION="ELAS"),
    INCREMENT=_F(LIST_INST=L_INST),
    NEWTON=_F(MATRICE="ELASTIQUE", REAC_INCR=0),
)

RESU = CALC_CHAMP(reuse=RESU, CONTRAINTE=("SIGM_ELNO"), RESULTAT=RESU)

RESU = CALC_CHAMP(reuse=RESU, CONTRAINTE="SIGM_NOEU", RESULTAT=RESU)


SIELNO = CREA_CHAMP(
    TYPE_CHAM="NOEU_SIEF_R", OPERATION="EXTR", RESULTAT=RESU, NOM_CHAM="SIGM_NOEU", INST=1.0
)


TEST_RESU(
    CHAM_NO=_F(
        CRITERE="RELATIF",
        REFERENCE="ANALYTIQUE",
        NOM_CMP="SIYY",
        GROUP_NO="G1_1",
        PRECISION=0.06,
        CHAM_GD=SIELNO,
        VALE_CALC=-2856.66564168,
        VALE_REFE=-2798.3,
    )
)

TEST_RESU(
    CHAM_NO=_F(
        CRITERE="RELATIF",
        REFERENCE="ANALYTIQUE",
        NOM_CMP="SIYY",
        GROUP_NO="G1_2",
        PRECISION=0.06,
        CHAM_GD=SIELNO,
        VALE_CALC=-2953.18997079,
        VALE_REFE=-2798.3,
    )
)

FIN()
