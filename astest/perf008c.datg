# -*- coding: utf-8 -*-
import math
import GEOM
import salome
salome.salome_init()
from salome.geom import geomBuilder
geompy = geomBuilder.New(salome.myStudy)
import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)

geo = geompy

#############Chemin pour l'export du maillage resultat############
ExportPATH="/home/PHIMECA/maillages/"
##################################################################

# Parameters
# ----------

radius =  50
height = 200

disrad = 22
disarc = 28
disz = 50

# Build a cylinder
# ----------------

base = geo.MakeVertex(0, 0, 0)
sensp=1
sensm=-1
directionp = geo.MakeVectorDXDYDZ(0, 0, sensp)
directionm = geo.MakeVectorDXDYDZ(0, 0, sensm)

cylinderp = geo.MakeCone(base, directionp, radius, 2*radius, height)
cylinderm = geo.MakeCone(base, directionm, radius, 2*radius, height)
cylinder = geo.MakeCompound([cylinderp, cylinderm])

geo.addToStudy(cylinder, "cylinder")

# Build blocks
# ------------

size = radius/2.0

dirboxp=sensp*height
dirboxm=sensm*height

box_rotp = geo.MakeBox(-size, -size, 0,  +size, +size, dirboxp)
box_rotm = geo.MakeBox(-size, -size, 0,  +size, +size, dirboxm)
box_axis = geo.MakeLine(base, directionp)
boxp = geo.MakeRotation(box_rotp, box_axis, math.pi/4)
boxm = geo.MakeRotation(box_rotm, box_axis, math.pi/4)

box = geo.MakeCompound([boxp, boxm])

hole = geo.MakeCut(cylinder, box)

plane_trim = 2000

plane_a = geo.MakePlane(base, geo.MakeVectorDXDYDZ(1, 0, 0), plane_trim)
plane_b = geo.MakePlane(base, geo.MakeVectorDXDYDZ(0, 1, 0), plane_trim)

blocks_part = geo.MakePartition([hole], [plane_a, plane_b], [], [], geo.ShapeType["SOLID"])
blocks_list = [box] + geo.SubShapeAll(blocks_part, geo.ShapeType["SOLID"])
blocks_all = geo.MakeCompound(blocks_list)
blocks = geo.MakeGlueFaces(blocks_all, 0.0001)

geo.addToStudy(blocks, "cylinder:blocks")

# Build geometric groups
# ----------------------

def group(name, shape, type, base=None, directionp=None):
    t = geo.ShapeType[type]
    g = geo.CreateGroup(shape, t)

    geo.addToStudy(g, name)
    g.SetName(name)

    if base!=None:
        l = geo.GetShapesOnPlaneWithLocationIDs(shape, t, directionp, base, GEOM.ST_ON)
        geo.UnionIDs(g, l)

    return g
base
up= geo.MakeVertex(0, 0, height)
down = geo.MakeVertex(0, 0, -height)

group_a = group("BASE_A", blocks, "FACE", down, directionm)

group_b = group("BASE_B", blocks, "FACE", up, directionp)

group_1 = group("MASSIF", blocks, "SOLID")
group_1_all = geo.SubShapeAllIDs(blocks, geo.ShapeType["SOLID"])
geo.UnionIDs(group_1, group_1_all)
group_1_box = geo.GetBlockNearPoint(blocks, down)
geo.DifferenceList(group_1, [group_1_box])


POINT_A = geo.CreateGroup(blocks, geompy.ShapeType["VERTEX"])
pt_A = geo.GetPoint(blocks, -2*radius, 0, -height, 1e-6)
pt_A_ID = geo.GetSubShapeID(blocks,pt_A)
geo.UnionIDs(POINT_A, [pt_A_ID])
geo.addToStudy(POINT_A, "POINT_A")

POINT_B = geo.CreateGroup(blocks, geompy.ShapeType["VERTEX"])
pt_B = geo.GetPoint(blocks, -2*radius, 0, height, 1e-6)
pt_B_ID = geo.GetSubShapeID(blocks,pt_B)
geo.UnionIDs(POINT_B, [pt_B_ID])
geo.addToStudy(POINT_B, "POINT_B")

POINT_C = geo.CreateGroup(blocks, geompy.ShapeType["VERTEX"])
pt_C = geo.GetPoint(blocks,  2*radius, 0, -height, 1e-6)
pt_C_ID = geo.GetSubShapeID(blocks,pt_C)
geo.UnionIDs(POINT_C, [pt_C_ID])
geo.addToStudy(POINT_C, "POINT_C")

# Mesh the blocks with hexahedral
# -------------------------------

def discretize(x, y, z,  n, s=blocks):
    p = geo.MakeVertex(x, y, z)
    e = geo.GetEdgeNearPoint(s, p)
    geo.addToStudy(e, str(n))
    a = hexa.Segment(e)
    a.NumberOfSegments(n)
    a.Propagation()

hexa = smesh.Mesh(blocks)

hexa_1d = hexa.Segment()
hexa_1d.NumberOfSegments(1)

discretize(+radius        , +radius, -height,   disarc)
discretize(+radius        , +radius,  height,   disarc)
discretize(-radius        , +radius, -height,   disarc)
discretize(-radius        , +radius,  height,   disarc)
discretize((radius+size)/2,       0, -height,   disrad)
discretize((radius+size)/2,       0,  height,   disrad)
discretize((radius+size)/2,       0, -height/4, disz)
discretize((radius+size)/2,       0,  height/4, disz)
discretize(        +radius,       0, -height/4, disz)
discretize(        +radius,       0,  height/4, disz)

hexa.Quadrangle()
hexa.Hexahedron()

hexa.Compute()

hexa.Group(group_a)
hexa.Group(group_b)
hexa.Group(group_1)
hexa.Group(POINT_A)
hexa.Group(POINT_B)
hexa.Group(POINT_C)

########Export du maillage au format MMED########
hexa.ExportMED( r''+ExportPATH+'perf008c.mmed'+'', 0, SMESH.MED_V2_2 , 1 )

