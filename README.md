# Performance testcases for code_aster

**code_aster** source files are dispatched into several repositories.

- [src][1]: containing Python, C/C++,
  Fortran source files, its build scripts and most of the testcases,
- *validation*: few testcase files with proprietary datas,
- *data*: material datas that can not be freely distributed.
- [perf][2]: this one containing some testcase files used for
  performance benchmarking.


[1]: ../../../../src
[2]: ../../../../perf
